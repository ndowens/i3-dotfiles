# Nathan's dotfiles
# Forked from Ermal https://github.com/ErmalFerati/dotfiles

**Operating System**: Artix Linux  
**Display Server**: Xorg  
**Window Manager**: i3wm  
**Bar**: Polybar  
**Application Launcher**: Rofi and dmenu  
**Compositor**: Compton  

Repository content:
* i3 config
* Polybar config
* Rofi config and theme
* Bashrc config
* Compton config
* Util scripts

